*** Setting ***
Library    SeleniumLibrary

*** Test Cases ***

Ouvrir navigateur
    ouvrir chrome
    Set Browser Implicit wait        5
    Sleep        2
    Close Browser
    
Go to linkedin
    ouvrir linkedin
    
recherche
    ouvrir chrome
    Input Text    name=q    Formation FMFP
    Press Keys    None    ENTER
    Sleep    2
    close Browser
    
*** Variables ***
${URL_GOOGLE}        https://www.google.com
${URL_LINKEDIN}        https://fr.linkedin.com/        Chrome
${NAVIGATEUR}        Chrome

*** Keywords ***
ouvrir chrome
    Open Browser        ${URL_GOOGLE}    ${NAVIGATEUR}
ouvrir linkedin
    Open Browser     ${URL_LINKEDIN}    ${NAVIGATEUR}